import React, { useState } from 'react';
import './App.css';
import { Button, Modal } from 'react-bootstrap';

function App() {
  const [fullName, setFullName] = useState("");
  const [birthDate, setBirthDate] = useState("");
  const [initial, setInitial] = useState("");
  const [age, setAge] = useState("");
  const [modalVisible, setModalVisible] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();

    // Kirim permintaan POST ke server Node.js
    const response = await fetch("/correspondent", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ fullname: fullName, birthdate: birthDate }),
    });

    if (response.ok) {
      const data = await response.json();
      setInitial(data.initial);
      setAge(data.age);
      setModalVisible(true);
    }
  };

  const closeModal = () => {
    setModalVisible(false);
  };

  return (
    <div className="App">
      <form onSubmit={handleSubmit}>
        <div className='container'>
        <div className='row'>

          <div className='col-4'>
            <div className='form-group'>
              <label> Full Name </label>
              <input className='form-control' type='text' value={fullName} onChange={(e) => setFullName(e.target.value)} required />
            </div>
            <div className='form-group'>
              <label> Birth Date </label>
              <input className='form-control' type='date' value={birthDate} onChange={(e) => setBirthDate(e.target.value)} required />
            </div>
            <button type='submit' className='btn'>Submit</button>
          </div>
</div>
        </div>

      </form>

      {modalVisible && (
        <Modal.Dialog>
          <Modal.Body>
            <p>Full Name Initial: {initial}</p>
            <p>Current Age: {age}</p>
            <button >OK</button>
          </Modal.Body>

          <Modal.Footer>
            <Button onClick={closeModal}>Close</Button>
          </Modal.Footer>
        </Modal.Dialog>
      )}
    </div>
  );
}

export default App;
